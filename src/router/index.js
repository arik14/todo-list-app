import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '@/components/HomePage'
import TodoPage from '@/components/TodoPage'
import TodoDaily from '@/components/TodoDaily'
import TodoWeekly from '@/components/TodoWeekly'

Vue.use(Router)

export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'HomePage',
			component: HomePage
		},
		{
			path: '/main',
			name: 'TodoPage',
			component: TodoPage,
			children: [
				{
					path: 'daily',
					component: TodoDaily
				},
				{
					path: 'weekly',
					component: TodoWeekly
				}
			]
		}
	]
})
