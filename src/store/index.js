import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
	dailyTasks: {
		Sunday: [],
		Monday: [],
		Tuesday: [],
		Wednesday: [],
		Thursday: [],
		Friday: [],
		Saturday: [],
	},
	weeklyTasks: {
		'Week 1': [],
		'Week 2': [],
		'Week 3': [],
		'Week 4': [],
		'Week 5': []
	},
	selectedDay: 'Sunday',
	selectedWeek: 'Week 1'
}

const getters = {
	dailyTasks: state => state.dailyTasks,
	weeklyTasks: state => state.weeklyTasks,
	selectedDay: state => state.selectedDay,
	selectedWeek: state => state.selectedWeek
}

const actions = {}

const mutations = {
	show_daily_tasks (state, payload) {
		state.selectedDay = payload.dayName
	},
	add_new_daily_task (state, payload) {
		let dayName = payload.dayName
		let newTask = Object.assign({}, payload.newTask)

		state.dailyTasks[dayName].push(newTask)
	},
	delete_daily_task (state, payload) {
		let { dayName, taskID } = payload
		let indexToDelete = state.dailyTasks[dayName].findIndex(item => item.taskID === taskID)

		state.dailyTasks[dayName].splice(indexToDelete, 1)
	},
	set_daily_task_done (state, payload) {
		let { dayName, indexTask } = payload

		state.dailyTasks[dayName][indexTask].isCompleted = true
	},
	edit_daily_task (state, payload) {
		let { dayName, indexTask } = payload

		state.dailyTasks[dayName][indexTask].isEditing = true
	},
	save_daily_task (state, payload) {
		let { dayName, indexTask, taskData } = payload

		state.dailyTasks[dayName][indexTask].isEditing = false
		state.dailyTasks[dayName][indexTask].task = taskData
	},

	show_weekly_tasks (state, payload) {
		state.selectedWeek = payload.weekName
	},
	add_new_weekly_task (state, payload) {
		let weekName = payload.weekName
		let newTask = Object.assign({}, payload.newTask)

		state.weeklyTasks[weekName].push(newTask)
	},
	delete_weekly_task (state, payload) {
		let { weekName, taskID } = payload
		let indexToDelete = state.weeklyTasks[weekName].findIndex(item => item.taskID === taskID)

		state.weeklyTasks[weekName].splice(indexToDelete, 1)
	},
	set_weekly_task_done (state, payload) {
		let { weekName, indexTask } = payload

		state.weeklyTasks[weekName][indexTask].isCompleted = true
	},
	edit_weekly_task (state, payload) {
		let { weekName, indexTask } = payload

		state.weeklyTasks[weekName][indexTask].isEditing = true
	},
	save_weekly_task (state, payload) {
		let { weekName, indexTask, taskData } = payload

		state.weeklyTasks[weekName][indexTask].isEditing = false
		state.weeklyTasks[weekName][indexTask].task = taskData
	},
}

export default new Vuex.Store({
	state,
	getters,
	actions,
	mutations
})
